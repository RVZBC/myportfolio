import React from 'react';
import Typewriter from 'typewriter-effect';
import CV from '../../assets/CV.pdf';
import { FcDownload } from "react-icons/fc";
import './Home.css';


export default function Home () {
  return (
    <div className='container-fluid home'>
      <div className='container home-content'>
        <h1>Hi I'm a</h1>
        <h3>
            <Typewriter
            options={{
            strings: [
              'Software Developer',
              'MERN Stack Developer'              
              ],
            autoStart: true,
            loop: true,
            delay: 38
            }}
            />
        </h3>
            <div className='button-for-action'>
              <div className='hire-me-button'>
                Hire Me
              </div>
              <div className='download-cv-button'>
                <a href={CV} download="Rowell_CV.pdf">Download CV <FcDownload/></a>
              </div>
            </div>
      </div>
    </div>
  )
}
