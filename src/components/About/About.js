import React from "react";
import ProfilePic from '../../assets/profile-pic.jpg';
import './About.css';

export default function About () {
    return (
        <div className="container about-section">
            <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                    <div className="about-image">
                        <img src={ProfilePic} alt='Profile'/>
                    </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                    <div className="about-details">
                        <div className="about-title">
                            <h5>About Me</h5>
                            <span className="line"></span>
                        </div>
                        <p>A career shifter and a self-starter in web development. Aspiring to improve and develop my skills to provide better services in the Software Development field. I might be a starter but with grit and passion I could progress in a short time.</p>
                    </div>
                </div>

            </div>
        </div>
    )
}