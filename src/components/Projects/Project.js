import React from "react";
import ProjectList from "./ProjectList";
import './Project.css';

export default function Project () {

    const data = [
        {
            name: "Portfolio",
            description: "My first portfolio website that I deployed using reactjs",
            projectlink: "",
            tools: [
                {
                    toolname: "React JS" 
                },
                {
                    toolname: "CSS"
                },
                {
                    toolname: "Bootstrap"
                },
            ]
        },
        {
            name: "MERN Stack E-commerce",
            description: "",
            projectlink: "",
            tools: [
                {
                    toolname: "React JS" 
                },
                {
                    toolname: "Express JS"
                },
                {
                    toolname: "Node JS"
                },
                {
                    toolname: "Mongo DB"
                },
                {
                    toolname: "Bootstrap"
                },
            ]
        },
        {
            name: "MERN Stack Meditation Booking",
            description: "",
            projectlink: "",
            tools: [
                {
                    toolname: "React JS" 
                },
                {
                    toolname: "Express JS"
                },
                {
                    toolname: "Node JS"
                },
                {
                    toolname: "Mongo DB"
                },
                {
                    toolname: "Bootstrap"
                },
            ]
        },
    ]
    return (
        <div className="container">

            <div className="section-title">
                <h5>Projects</h5>
                <span className="line"></span>
            </div>

            <div className="row">
                {data.map((item, index) => (
                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12" key={index}>
                        <ProjectList {...item}/>
                    </div>
                ))}                
            </div>

        </div>
    )
}