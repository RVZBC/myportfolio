import React, {useState} from "react";
import { FcExpand, FcCollapse } from "react-icons/fc";

export default function ProjectList ({name, description, projectlink, tools}) {

    const [show, setShow] = useState(false);

    const handleExCol = () => {
        setShow(!show);
    }

    return (
        <div className={show ? "project-list-opened project-list" : "project-list"} onClick={handleExCol}
        onMouseEnter={() => setShow(true)}
        // onMouseLeave={() => setShow(false)}
        >

            <div className="title-and-collapse-optn">
                <h5>{name}</h5>
                <p>
                    {show ? <FcCollapse size={20}/> : <FcExpand size={20}/>}
                </p>
            </div>

            <div className="description"></div>

            {show ? <p>{description}</p> : <p>{description.substring(0, 0)}...<p style={{color:"green"}}>Read More</p></p>}

            <div className="row">
                {tools && tools.map((item, index) => (
                    <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12" key={index}>
                        <div className="tool-used-in-proj">
                            <p>{item.toolname}</p>
                        </div>
                    </div>
                ))}
            </div>
            <div className="live-demo-button">
                <a target="_blank" href={projectlink} rel="noreferrer noopener">Live Demo</a>
            </div>

            
        </div>
    )
}