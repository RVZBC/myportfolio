import React, {useState} from "react";

import './Tools.css';


export default function Tools () {

    const data = [
        {
            name: "Frontend Developer",
        },
        {
            name: "Backend Developer",
        },
        {
            name: "Full Stack Developer",
        },
        {
            name: "Mongo DB"
        },
        {
            name: "Express js"
        },
        {
            name: "React js"
        },
        {
            name: "Node js"
        },
        {
            name: "Vite js"
        },
        {
            name: "Javascript"
        },
        {
            name: "HTML"
        },
        {
            name: "CSS"
        },
        {
            name: "Tailwind CSS"
        },
        {
            name: "Bootstrap"
        },
        {
            name: "Laravel"
        },
        {
            name: "PHP"
        }
    ];

    const colors = [
        "#2AD7FA",
        "#788EE9",
        "#3CE8AE",
        "#FEDD55",
        "#FE4545",
        "#C853C1",
        "#F0EA39",
        "#7D7D75",
        "#2AD7FA",
        "#788EE9",
        "#3CE8AE",
        "#FEDD55",
        "#FE4545",
        "#C853C1",
        "#788EE9"
    ];

    const [collapse, setCollapse] = useState(6);

    const expand = () => {
        setCollapse((prev) => prev + 9)
    }

    const contract = () => {
        setCollapse((current) => current - 9)
    }

    return (
        <div className="container tools-section">

            <div className="section-title">
                <h5>Tools</h5>
                <span className="line"></span>
            </div>

            <div className="row">
                {data.slice(0,collapse).map((item, index) => (
                    <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12" key={index}>
                        <div className="tools-content">
                            <span className="tools-number" style={{backgroundColor: colors[index]}}>
                                {index + 1}
                            </span>
                            <p>{item.name}</p>
                        </div>                        
                    </div>
                ))}
            </div>

            {collapse >= data.length ? (<span className="load-more-tool" onClick={contract}>Collapse</span>) : (
                <span className="load-more-tool" onClick={expand}>Expand</span>
            )} 

        </div>
    )
}