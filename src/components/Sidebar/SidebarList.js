import React from 'react';
import Profilepic from '../../assets/profile-pic.jpg';
import { FcHome, FcPortraitMode, FcTimeline, FcTabletAndroid, FcTodoList, FcContacts} from "react-icons/fc";
import './SidebarList.css';

export default function SidebarList ({expandSidebar}) {
  return (
    <>
        {expandSidebar ? (
            <div className='navbar-items'>

                <div className='sidebar-profile-pic'>
                    <img src={Profilepic} alt='profile'/>
                </div>

                <ul>
                    <li className='nav-item'><FcHome size={25} />Home</li>
                    <li className='nav-item'><FcPortraitMode size={25} />About</li>
                    <li className='nav-item'><FcTimeline size={25} />Experience</li>
                    <li className='nav-item'><FcTabletAndroid size={25} />Tools</li>
                    <li className='nav-item'><FcTodoList size={25} />Projects</li>
                    <li className='nav-item'><FcContacts size={25} />Contact</li>
                </ul>

            </div>
        ) : (
            <div className='navbar-items-only-icons'>
                <ul>
                    <li className='nav-item'><FcHome size={25} /></li>
                    <li className='nav-item'><FcPortraitMode size={25} /></li>
                    <li className='nav-item'><FcTimeline size={25} /></li>
                    <li className='nav-item'><FcTabletAndroid size={25} /></li>
                    <li className='nav-item'><FcTodoList size={25} /></li>
                    <li className='nav-item'><FcContacts size={25} /></li>
                </ul>
            </div>
        )}
    </>
  )
}
